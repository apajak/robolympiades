#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2023/03/07
# version ='1.0'
# ---------------------------------------------------------------------------
""" NEMA 17 (17HS4023) Emulator Class for simulations"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
from time import sleep
import threading
# ---------------------------------------------------------------------------
# Basic stepper motor parameters
# mymotortest.motor_go(True, "Full", 200, 0.005, False, 0.05)
# ---------------------------------------------------------------------------
class Stepper:
    def __init__(
        self, current_step, current_direction, totalRotationDegree, total_steps, status
    ):
        self.current_step = current_step
        self.current_direction = current_direction
        self.totalRotationDegree = totalRotationDegree
        self.total_steps = total_steps
        self.status = status

    def move(self, clockwise, steptype, steps, stepdelay, verbose, initdelay):
        # Check if the stepper is moving
        if self.status == "moving":
            pass
        else:
            self.status = "moving"
        # Check if the steps is positive
        if steps < 0:
            print("Steps must be positive")
            return
        # Check if the stepdelay is positive
        if stepdelay < 0:
            print("Stepdelay must be positive")
            return
        # Check if the initdelay is positive
        if initdelay < 0:
            print("Initdelay must be positive")
            return
        # Check if the steptype is supported
        if steptype == "Full":
            stepIncrementDegree = 360 / 200
        elif steptype == "Half":
            stepIncrementDegree = 360 / 400
        elif steptype == "1/4":
            stepIncrementDegree = 360 / 800
        elif steptype == "1/8":
            stepIncrementDegree = 360 / 1600
        elif steptype == "1/16":
            stepIncrementDegree = 360 / 3200
        else:
            print("Steptype not supported")
            return

        sleep(initdelay)
        # Clockwise
        if clockwise:
            for step in range(steps):
                self.total_steps += 1
                self.totalRotationDegree += stepIncrementDegree
                self.current_direction = "forward"
                self.current_step = step
                if steps == 1:
                    pass
                else:
                    sleep(stepdelay)
            if steps > 1:
                self.status = "stopped"

        elif not clockwise:
            for step in range(steps):
                self.total_steps += 1
                self.totalRotationDegree -= stepIncrementDegree
                self.current_direction = "backward"
                self.current_step = step
                if steps == 1:
                    pass
                else:
                    sleep(stepdelay)
            if steps > 1:
                self.status = "stopped"
