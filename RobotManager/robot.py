#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2023/03/07
# version ='1.0'
# ---------------------------------------------------------------------------
""" NEMA 17 (17HS4023) Raspberry Pi Manager Emulator for simulations"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import time
import numpy as np
from time import sleep
import threading
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from stepper import Stepper

# ---------------------------------------------------------------------------
# Basic stepper motor parameters
# mymotortest.motor_go(True, "Full", 200, 0.005, False, 0.05)
# ---------------------------------------------------------------------------
# global variables
stepper_initState = None
stepperState = None
stepper_endState = None
run = True
weel_radius = 3  # cm
weel_perimetter = 2 * np.pi * weel_radius


class Robot:
    def __init__(
        self, x, y, theta, width, length, wheel_radius, stepper_1, stepper_2, status
    ):
        self.x = x
        self.y = y
        self.theta = theta
        self.width = width
        self.length = length
        self.wheel_radius = wheel_radius
        self.stepper_1 = stepper_1
        self.stepper_2 = stepper_2
        self.status = status

    def check_status(self):
        while True:
            if self.stepper_1.status == "moving" or self.stepper_2.status == "moving":
                self.status = "moving"
            else:
                self.status = "moving"
            print(f"Robot status: {self.status}")
            return self.status

    def get_postion(self):
        while True:
            if self.status == "moving":
                totalRotation_1 = self.stepper_1.totalRotationDegree
                totalRotation_2 = self.stepper_2.totalRotationDegree
                distance_1 = round(totalRotation_1 / 360 * weel_perimetter, 2)
                distance_2 = round(totalRotation_2 / 360 * weel_perimetter, 2)
                # get wheels position
                wheel_1_x = 0
                wheel_1_y = distance_1
                wheel_2_x = self.width
                wheel_2_y = distance_2
                # get robot position
                self.x = (wheel_1_x + wheel_2_x) / 2
                self.y = (wheel_1_y + wheel_2_y) / 2
                self.theta = round(
                    np.arctan2(wheel_2_y - wheel_1_y, wheel_2_x - wheel_1_x)
                    * 180
                    / np.pi,
                    2,
                )
                #print(f"Robot position: x={self.x}, y={self.y}, theta={self.theta}")
            else:
                #print("Robot is not moving")
                pass
            sleep(0.1)

    def thread_get_postion(self):
        thread = threading.Thread(target=self.get_postion).start()
        return thread

    def thread_check_status(self):
        thread = threading.Thread(target=self.check_status).start()
        return thread

    def move_forward(self, distance, maxSpeed, steptype, verbose, initdelay):
        # Check if the robot is moving
        if self.status == "moving":
            print("The robot is already moving")
        # Check if the distance is positive
        if distance < 0:
            print("Distance must be positive")
            return
        # Check if the speed is positive
        if maxSpeed < 0:
            print("Speed must be positive")
            return
        # Check if the steptype is supported
        if steptype == "Full":
            oneTurn = 200
        elif steptype == "Half":
            oneTurn = 400
        elif steptype == "1/4":
            oneTurn = 800
        elif steptype == "1/8":
            oneTurn = 1600
        elif steptype == "1/16":
            oneTurn = 3200
        else:
            print("Steptype not supported")
            return

        # Calculate the number of steps for the distance
        steps = int(distance / weel_perimetter * oneTurn)
        # Calculate the delay between each step for the setting speed
        duration = distance / maxSpeed
        minDelay = duration / steps
        print(f"minDelay{minDelay}")
        maxDelay = 0.05
        # calculate the logaritmic delay for reach the minDelay
        accelerationSteps = int((steps / 100) * 20)
        deccelerationSteps = int((steps / 100) * 20)
        maxSpeedSteps = steps - accelerationSteps - deccelerationSteps
        print(f"accelerationSteps: {accelerationSteps}")
        print(f"deccelerationSteps: {deccelerationSteps}")
        print(f"maxSpeedSteps: {maxSpeedSteps}")
        # create a logspace array for the acceleration betwin minDelay and maxDelay
        logDelay_acceleration = np.logspace(
            np.log10(maxDelay), np.log10(minDelay), accelerationSteps
        )
        # create a logspace array for the decceleration betwin minDelay and maxDelay
        logDelay_deceleration = np.logspace(
            np.log10(minDelay), np.log10(maxDelay), deccelerationSteps
        )
        if maxSpeedSteps > 0:
            logDelay_maxSpeed = np.full(maxSpeedSteps, minDelay)
            logDelay_table = np.append(logDelay_acceleration, logDelay_maxSpeed)
            logDelay_table = np.append(logDelay_table, logDelay_deceleration)
        else:
            logDelay_table = np.append(logDelay_acceleration, logDelay_deceleration)
        print(f"len(logDelay_table): {len(logDelay_table)}")
        index = 0
        startTime = time.time()
        for delay in logDelay_table:
            self.stepper_1.move(True, steptype, 1, delay, verbose, initdelay)
            self.stepper_2.move(True, steptype, 1, delay, verbose, initdelay)
            index += 1
            time.sleep(delay)
        endTime = time.time()
        print(f"duration: {endTime - startTime}")
        print(f"speed: {distance / (endTime - startTime)}")

    def move_backward(self, distance, maxSpeed, steptype, verbose, initdelay):
        # Check if the robot is moving
        if self.status == "moving":
            print("The robot is already moving")
        # Check if the distance is positive
        if distance < 0:
            print("Distance must be positive")
            return
        # Check if the speed is positive
        if maxSpeed < 0:
            print("Speed must be positive")
            return
        # Check if the steptype is supported
        if steptype == "Full":
            oneTurn = 200
        elif steptype == "Half":
            oneTurn = 400
        elif steptype == "1/4":
            oneTurn = 800
        elif steptype == "1/8":
            oneTurn = 1600
        elif steptype == "1/16":
            oneTurn = 3200
        else:
            print("Steptype not supported")
            return

        # Calculate the number of steps for the distance
        steps = int(distance / weel_perimetter * oneTurn)
        # Calculate the delay between each step for the setting speed
        duration = distance / maxSpeed
        minDelay = duration / steps
        print(f"minDelay{minDelay}")
        maxDelay = 0.05
        # calculate the logaritmic delay for reach the minDelay
        accelerationSteps = int((steps / 100) * 20)
        deccelerationSteps = int((steps / 100) * 20)
        maxSpeedSteps = steps - accelerationSteps - deccelerationSteps
        print(f"accelerationSteps: {accelerationSteps}")
        print(f"deccelerationSteps: {deccelerationSteps}")
        print(f"maxSpeedSteps: {maxSpeedSteps}")
        # create a logspace array for the acceleration betwin minDelay and maxDelay
        logDelay_acceleration = np.logspace(
            np.log10(maxDelay), np.log10(minDelay), accelerationSteps
        )
        # create a logspace array for the decceleration betwin minDelay and maxDelay
        logDelay_deceleration = np.logspace(
            np.log10(minDelay), np.log10(maxDelay), deccelerationSteps
        )
        if maxSpeedSteps > 0:
            logDelay_maxSpeed = np.full(maxSpeedSteps, minDelay)
            logDelay_table = np.append(logDelay_acceleration, logDelay_maxSpeed)
            logDelay_table = np.append(logDelay_table, logDelay_deceleration)
        else:
            logDelay_table = np.append(logDelay_acceleration, logDelay_deceleration)
        print(logDelay_table)
        print(f"len(logDelay_table): {len(logDelay_table)}")
        index = 0
        startTime = time.time()
        for delay in logDelay_table:
            self.stepper_1.move(False, steptype, 1, delay, verbose, initdelay)
            self.stepper_2.move(False, steptype, 1, delay, verbose, initdelay)
            index += 1
            time.sleep(delay)
        endTime = time.time()
        print(f"duration: {endTime - startTime}")
        print(f"speed: {distance / (endTime - startTime)}")



