#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2023/03/07
# version ='1.0'
# ---------------------------------------------------------------------------
""" NEMA 17 (17HS4023) Raspberry Pi Manager Emulator for simulations"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import time
import numpy as np
from time import sleep
import threading
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from stepper import Stepper
from robot import Robot

# ---------------------------------------------------------------------------
# Basic stepper motor parameters
# mymotortest.motor_go(True, "Full", 200, 0.005, False, 0.05)
# ---------------------------------------------------------------------------
run = True

def itinary(robot):
    global run
    robot.thread_check_status()
    robot.thread_get_postion()
    robot.move_forward(40, 10, "Full", True, 0)
    robot.move_backward(100, 10, "Full", False, 0)
    run = False


def main():
    global run
    print("Start of program")
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlim(-100, 100)
    ax.set_ylim(-100, 100)
    ax.set_aspect("equal")
    ax.grid()

    stepper_1 = Stepper(0, "forward", 0, 0, "stopped")
    stepper_2 = Stepper(0, "forward", 0, 0, "stopped")
    robot = Robot(0, 0, 0, 24.2, 20.0, 3, stepper_1, stepper_2, "stopped")

    threading.Thread(target=itinary, args=(robot,)).start()

    while run:
        print(f"Robot position: ({robot.x}, {robot.y})")
        (robot_position,) = ax.plot(robot.x, robot.y, "ro")
        plt.pause(0.01)
        robot_position.remove()

    plt.close()
    print("End of program")


if __name__ == "__main__":
    main()
