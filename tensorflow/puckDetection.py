#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2023/03/22
# version ='1.0'
# ---------------------------------------------------------------------------
""" Black puck detection with My tensorflow model"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import tensorflow as tf
import numpy as np
import cv2
import time
from object_detection.utils import ops as utils_ops
from object_detection.utils import visualization_utils as vis_util
import os
from PIL import Image
import tkinter as tk
def main():
    # ---------------------------------------------------------------------------
    PATH_TO_SAVED_MODEL = "./my_model/saved_model"
    print('Loading model...', end='')
    start_time = time.time()

    # Load saved model and build the detection function
    model = tf.saved_model.load(PATH_TO_SAVED_MODEL)

    end_time = time.time()
    elapsed_time = end_time - start_time
    print('Done! Took {} seconds'.format(elapsed_time))

    # ---------------------------------------------------------------------------
    # Load label map data (for plotting)
    # Label maps map indices to category names, so that when our convolution
    # network predicts `5`, we know that this corresponds to `airplane`.
    # Here we use internal utility functions, but anything that returns a
    # dictionary mapping integers to appropriate string labels would be fine

    label_map = {1: {'id': 1, 'name': 'puck'}}
    # ---------------------------------------------------------------------------
    # Load cam image
    # ---------------------------------------------------------------------------
    video = cv2.VideoCapture(0)
    while True:
        ret, frame = video.read()
        # ---------------------------------------------------------------------------
        # The input needs to be a tensor, convert it using `tf.convert_to_tensor`.
        input_tensor = tf.convert_to_tensor(frame)
        # The model expects a batch of images, so add an axis with `tf.newaxis`.
        input_tensor = input_tensor[tf.newaxis, ...]
        # ---------------------------------------------------------------------------
        # Run inference
        # ---------------------------------------------------------------------------
        print('Running inference for {}... '.format(frame), end='')
        start_time = time.time()
        detections = model(input_tensor)
        end_time = time.time()
        elapsed_time = end_time - start_time
        print('Done! Took {} seconds'.format(elapsed_time))
        # ---------------------------------------------------------------------------
        # All outputs are batches tensors.
        # Convert to numpy arrays, and take index [0] to remove the batch dimension.
        # We're only interested in the first num_detections.
        num_detections = int(detections.pop('num_detections'))
        detections = {key: value[0, :num_detections].numpy()
                      for key, value in detections.items()}
        print(detections)
        detections['num_detections'] = num_detections
        # detection_classes should be ints.
        detections['detection_classes'] = detections['detection_classes'].astype(np.int64)
        # Handle models with masks:
        if 'detection_masks' in detections:
            # Reframe the the bbox mask to the image size.
            detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
                detections['detection_masks'], detections['detection_boxes'],
                frame.shape[0], frame.shape[1])
            detection_masks_reframed = tf.cast(detection_masks_reframed > 0.5,
                                               tf.uint8)
            detections['detection_masks_reframed'] = detection_masks_reframed.numpy()
        # ---------------------------------------------------------------------------
        # Visualization of the results of a detection.
        # ---------------------------------------------------------------------------
        frame = vis_util.visualize_boxes_and_labels_on_image_array(
            frame,
            detections['detection_boxes'],
            detections['detection_classes'],
            detections['detection_scores'],
            label_map,
            use_normalized_coordinates=True,
            max_boxes_to_draw=200,
            min_score_thresh=0.95,
            agnostic_mode=False)
        # ---------------------------------------------------------------------------
        # Display output
        # ---------------------------------------------------------------------------
        cv2.imshow('Object detector', frame)
        # Press 'q' to quit
        if cv2.waitKey(1) == ord('q'):
            break
    # Clean up
    video.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()