import _thread
import time
import machine
from servo import Servo
from hcsr04 import HCSR04
from time import sleep


# global vars
servo_pin = machine.Pin(13)
my_servo = Servo(servo_pin)
sensor = HCSR04(trigger_pin=5, echo_pin=18, echo_timeout_us=10000)
flag = False
distance: float = 0


def ultraSonicThread():
    global flag
    global distance
    print(f"ultraSonicThread started")
    while True:
        distance = sensor.distance_cm()
        sleep(0.2)
        if flag:
            print(f"ultraSonicThread stopped")
            break


def servoThread():
    print(f"servoThread started")
    global flag
    global distance
    while True:
        # conver cm to angle for 100 cm = 180 degrees
        angle = int(distance * 1.8)
        if angle > 180:
            angle = 180
        my_servo.write_angle(angle)
        sleep(0.2)
        if flag:
            print(f"servoThread stopped")
            break


def displayThread():
    print(f"displayThread started")
    global flag
    global distance
    while True:
        print(f"- Distance: {distance} cm & Angle: {int(distance * 1.8)}")
        sleep(0.2)
        if flag:
            print(f"displayThread stopped")
            break


if __name__ == "__main__":
    print("Welcome to the servo test program")
    print("Press Ctrl-C to stop the program")
    input("Press Enter to start the program")
    try:
        _thread.start_new_thread(ultraSonicThread, ())
        _thread.start_new_thread(servoThread, ())
        _thread.start_new_thread(displayThread, ())
        while True:
            pass
    except KeyboardInterrupt:
        flag = True
        print("KeyboardInterrupt")
        pass
    except Exception as e:
        print(e)
        pass
