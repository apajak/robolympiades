from rplidar import RPLidar
from time import sleep
import threading
from utils import polarToCartesian, equation_line, checkIfParallel, findDistance
import math

class Lidar:
    def __init__(self, port_name):
        self.lidar = RPLidar(port_name)
        self.scan = []
        self.oneTurnData = []
        self.oneTurnPoints = []
        self.detectedWalls = []
        self.previusMesure = [False, 1, 0.0, 0.1]
        self.lidartmp = []
        self.stop_scan = False
        self.measure = True
        self.message = "No data"
        self.PORT_NAME = port_name
        self.lidar.motor_speed = 10

    # port managment
    def connect(self):
        try:
            self.lidar.connect()
            print("Port is connected")
        except Exception as e:
            print(f"Port is not connected: {e}")

    def disconnect(self):
        try:
            self.lidar.disconnect()
            print("Port is disconnected")
        except Exception as e:
            print(f"Port is not disconnected: {e}")

    # general managment
    def get_info(self):
        try:
            print(f"Motor is on: {self.lidar.motor_running}")
            print(f"Health: {self.lidar.get_health()}")
            print(f"Info: {self.lidar.get_info()}")
        except Exception as e:
            print(e)

    # motor managment:
    def motor_on(self):
        try:
            self.lidar.start_motor()
            print("Motor is on")
        except Exception as e:
            print(e)

    def motor_off(self):
        try:
            self.lidar.stop_motor()
            print("Motor is off")
        except Exception as e:
            print(e)

    def motor_is_on(self):
        print(self.lidar.motor_running)
        print(self.lidar._motor_speed)
        return self.lidar.motor_running

    def get_scan(self):
        for i, scan in enumerate(self.lidar.iter_scans()):
            self.scan = scan
            if self.stop_scan:
                break


    # mesure managment
    def stop_measure(self):
        self.measure = False
    def get_measure(self):
        # get data from lidar
        turnData = []
        turnPoints = []
        while self.measure:
            for quality, angle, distance in self.scan:
                if quality > 0 and distance > 0:
                    turnData.append([quality, angle, distance])
                    turnPoints.append(
                        polarToCartesian(angle, distance)
                    )
                else:
                    pass
            self.oneTurnData = turnData
            self.oneTurnPoints = turnPoints
            turnData = []
            turnPoints = []
            sleep(0.2)

    def start_scan(self):
        self.stop_scan = False
        scan_thread = threading.Thread(target=self.get_scan, args=())
        scan_thread.start()
    def start_measure(self):
        measure_thread = threading.Thread(target=self.get_measure, args=())
        measure_thread.start()

    def clear(self):
        self.lidar.reset()

    def distanceLimit(self, data, limit):
        return [mesure for mesure in data if mesure[3] < limit]

    def neightborFinder(self, oneTurnPoints, distance_limit=50) -> []:
        neightborList = []
        neightbor = []
        iterator = iter(oneTurnPoints)
        point1 = next(iterator, None)
        while point1 is not None:
            point2 = next(iterator, None)
            if point2 is not None:
                distance = findDistance(point1, point2)
                if distance < distance_limit:
                    neightbor.append([point1, point2])
                elif neightbor:
                    neightborList.append(neightbor)
                    neightbor = []
                point1 = point2
            else:
                break
        neightborList = list(filter(lambda x: len(x) > 3, neightborList))
        if neightbor:
            neightborList.append(neightbor)
        return neightborList

    def isLine(self,neighbors):
        point1 = neighbors[0][0]
        point2 = neighbors[(len(neighbors)//2)][0]
        point3 = neighbors[-1][-1]
        return checkIfParallel(equation_line(point1, point2), equation_line(point1, point3))

    def findWalls(self):
        while True:
            walls = []
            neightborList = self.neightborFinder(self.oneTurnPoints)
            for neightbor in neightborList:
                if self.isLine(neightbor):
                    wall = [neightbor[0][0], neightbor[-1][-1]]
                    walls.append(wall)

            self.detectedWalls = walls
            sleep(0.2)

    def start_wallDetection(self):
        findWalls_thread = threading.Thread(target=self.findWalls, args=())
        findWalls_thread.start()