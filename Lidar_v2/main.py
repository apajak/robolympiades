from lidarManager import Lidar
from emulatorLidarManager import ELidar
from time import sleep
import matplotlib.pyplot as plt

# global variables
#PORT_NAME = "/dev/cu.SLAB_USBtoUART"
PORT_NAME = "/dev/ttyUSB0"
Emulator = False
Draw = True


def main():
    print("Welcome to the lidar test program")
    if Emulator:
        lidar = ELidar()
    else:
        lidar = Lidar(PORT_NAME)

    lidar.connect()
    # get info
    lidar.get_info()
    lidar.motor_on()

    # for emulator only
    if Emulator:
        lidar.start_EmulatorThread()
        print("wait for Emulator data stream...")
        sleep(1)

    # create a thread to get the mesure
    lidar.start_scan()
    # print("scan data is ready")
    lidar.start_measure()
    # create a thread to get the walls
    lidar.start_wallDetection()

    if Draw:
        plt.ion()
        plt.show()
        # set plot limits
        plt.xlim(-2000, 2000)
        plt.ylim(-2000, 2000)

        while True:
            # get latest lidar data
            one_turn_point = lidar.oneTurnPoints
            if one_turn_point is None:
                continue

            # clear previous plot
            plt.clf()
            # set plot limits
            plt.xlim(-2000, 2000)
            plt.ylim(-2000, 2000)
            # plot lidar point
            plt.scatter(0, 0, c='green', s=100)
            # plot lidar data points
            for i in range(len(one_turn_point)):
                # add point
                plt.scatter(one_turn_point[i][0], one_turn_point[i][1], c='blue')

            neighbors = lidar.neightborFinder(one_turn_point)
            # plot walls
            walls = lidar.detectedWalls
            for wall in walls:
                start = wall[0]
                end = wall[1]
                plt.plot([start[0], end[0]], [start[1], end[1]], c='red')

            # update plot
            plt.pause(0.001)




# main
if __name__ == "__main__":
    main()
