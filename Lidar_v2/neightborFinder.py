import numpy as np
import math
import matplotlib.pyplot as plt
from utils import polarToCartesian, findDistance, findAngle, equation_line, checkIfParallel,cartesianToPolar,findNearestPoint

oneTurnData = [[False, 17, 0.72, 4106.75], [False, 17, 5.72, 797.75], [False, 29, 8.17, 774.25], [False, 33, 9.34, 771.25], [False, 31, 11.64, 781.0], [False, 13, 17.42, 3747.0], [False, 16, 19.78, 4165.75], [False, 15, 22.16, 4231.0], [False, 13, 24.56, 3946.25], [False, 15, 26.92, 3939.75], [False, 10, 29.3, 3869.5], [False, 10, 31.67, 4173.25], [False, 15, 50.41, 571.75], [False, 21, 53.03, 548.0], [False, 24, 55.36, 525.75], [False, 24, 58.06, 505.75], [False, 23, 59.22, 497.25], [False, 27, 61.8, 481.5], [False, 24, 65.61, 460.25], [False, 28, 67.92, 449.25], [False, 30, 69.12, 443.75], [False, 24, 71.72, 433.0], [False, 25, 74.02, 425.5], [False, 27, 76.59, 417.75], [False, 25, 78.89, 411.5], [False, 29, 80.27, 408.5], [False, 26, 82.66, 403.0], [False, 29, 83.95, 400.75], [False, 30, 85.98, 397.5], [False, 33, 88.42, 394.25], [False, 35, 90.91, 392.0], [False, 41, 92.12, 390.75], [False, 47, 94.33, 389.25], [False, 47, 96.64, 388.75], [False, 46, 98.23, 388.75], [False, 47, 100.44, 390.0], [False, 48, 103.0, 391.0], [False, 38, 105.03, 392.25], [False, 31, 107.66, 395.0], [False, 30, 111.2, 400.0], [False, 27, 112.27, 402.0], [False, 27, 114.41, 407.25], [False, 27, 118.06, 414.25], [False, 27, 119.11, 418.75], [False, 26, 121.62, 425.75], [False, 25, 123.69, 433.75], [False, 28, 125.88, 443.0], [False, 27, 128.28, 453.75], [False, 23, 129.55, 459.0], [False, 25, 131.8, 471.5], [False, 10, 169.58, 2581.0], [False, 16, 176.48, 3872.25], [False, 11, 178.86, 4607.75], [False, 11, 180.03, 4586.5], [False, 10, 182.77, 2136.0], [False, 10, 187.16, 4523.75], [False, 10, 189.53, 4541.5], [False, 11, 191.92, 4600.75], [False, 10, 193.09, 4610.0], [False, 10, 197.84, 4658.5], [False, 9, 200.22, 4733.75], [False, 9, 202.69, 4783.75], [False, 10, 206.23, 4874.75], [False, 9, 212.19, 5067.75], [False, 12, 215.48, 1239.25], [False, 10, 218.0, 1056.0], [False, 9, 220.55, 926.0], [False, 10, 223.06, 891.75], [False, 14, 225.38, 887.0], [False, 19, 227.81, 883.75], [False, 17, 230.22, 896.75], [False, 13, 234.78, 1031.0], [False, 11, 237.19, 1042.5], [False, 13, 239.59, 1050.75], [False, 9, 241.86, 1099.5], [False, 17, 244.53, 3715.5], [False, 21, 250.05, 409.5], [False, 28, 252.55, 402.25], [False, 28, 255.19, 394.25], [False, 30, 256.05, 390.25], [False, 25, 258.52, 384.5], [False, 30, 262.45, 376.75], [False, 28, 263.44, 373.75], [False, 32, 266.2, 370.5], [False, 32, 268.33, 367.5], [False, 35, 270.62, 365.0], [False, 37, 273.12, 363.25], [False, 48, 275.88, 362.0], [False, 47, 277.0, 361.75], [False, 45, 279.38, 361.25], [False, 48, 281.62, 362.0], [False, 37, 283.62, 364.0], [False, 35, 286.09, 365.5], [False, 32, 288.75, 367.5], [False, 31, 290.75, 371.0], [False, 28, 293.5, 373.5], [False, 29, 295.59, 378.75], [False, 29, 298.02, 383.75], [False, 28, 299.98, 389.75], [False, 28, 302.47, 396.5], [False, 26, 304.84, 404.5], [False, 28, 307.14, 413.5], [False, 25, 308.3, 418.0], [False, 23, 310.8, 428.25], [False, 24, 312.86, 440.25], [False, 23, 314.03, 446.0], [False, 21, 316.42, 459.5], [False, 24, 318.41, 474.5], [False, 21, 321.02, 491.25], [False, 21, 322.98, 511.25], [False, 20, 325.33, 531.75], [False, 20, 327.8, 555.0], [False, 19, 330.0, 582.75], [False, 19, 332.11, 613.5], [False, 16, 336.3, 2776.0], [False, 13, 338.72, 2623.0], [False, 15, 341.11, 2634.75], [False, 15, 343.28, 4260.5], [False, 15, 345.67, 4166.75], [False, 16, 347.97, 4060.25], [False, 16, 350.33, 4267.5], [False, 16, 352.7, 4230.25], [False, 15, 353.89, 4222.5], [False, 9, 356.34, 3504.0], [False, 15, 358.64, 4122.25]]
oneTurnPoints = [[4106.425748284022, 51.60558429095831], [793.7778752749534, 79.50941594541234], [766.3919629796885, 110.02918512894605], [761.0252728035226, 125.16827335360973], [764.9384476100618, 157.57592255134944], [3575.147162797259, 1121.7538786840498], [3919.966390418832, 1409.7292507736204], [3918.4735539162907, 1595.9091475577907], [3589.2189753508383, 1640.242728830546], [3512.836900449144, 1783.705966055743], [3374.4720496760906, 1893.6653970421605], [3551.795188255086, 2191.0651526570396], [364.37027168828325, 440.60454787689906], [329.5654333030435, 437.82487957147566], [298.8459024132986, 432.55541738692995], [267.5573776472865, 429.1807453352344], [254.46420680685802, 427.206659538632], [227.53319328444954, 424.3476121692937], [190.05815682488458, 419.17533267634866], [168.8734437087236, 416.3019607327793], [158.157771179005, 414.6084682151216], [135.815223752636, 411.14866532316677], [117.14091394256253, 409.0577664348902], [96.88361139781517, 406.36021993118015], [79.29326475068025, 403.788098097478], [69.03873432129706, 402.6237737184844], [51.48609136538091, 399.6976136980501], [42.23757020412731, 398.5179420844831], [27.866612299734108, 396.5220068532594], [10.87053984304946, 394.10010639876856], [-6.225676758359963, 391.95055931698886], [-14.454834177630657, 390.4825479696848], [-29.388702530328228, 388.13897854194425], [-44.95140641940767, 386.14237472843774], [-55.648458354050995, 384.7464250422835], [-70.67024781458353, 383.5436299481786], [-87.95586224845123, 380.978695331027], [-101.72013992675534, 378.8311967529619], [-119.83032196478382, 376.38503415759806], [-144.64982803283692, 372.92952048620486], [-152.3466108818779, 372.0141262812552], [-168.30150654267757, 370.8465523575284], [-194.86151224500662, 365.55718231979677], [-203.7167976719318, 365.85656867998586], [-223.2135650888376, 362.54484806756153], [-240.60078477144702, 360.9020987295084], [-259.6376764726167, 358.93909922980083], [-281.10041889249294, 356.1904223845232], [-292.2688684887998, 353.9207658675549], [-314.2700597226169, 351.4919338504695], [-2538.4351747814603, 466.8060233461171], [-3864.944720225777, 237.74434188617664], [-4606.837970213288, 91.67321692356523], [-4586.499371292506, -2.4014856745507274], [-2133.5042508338497, -103.22602227100245], [-4488.473610391178, -563.842807280523], [-4478.822974761417, -751.9089112048672], [-4501.543782298759, -950.2652990335806], [-4490.211503681765, -1044.0788534416085], [-4434.499498772635, -1427.1777903909704], [-4442.020508388395, -1636.1060068213521], [-4413.513719616648, -1845.307754618622], [-4372.782732719042, -2154.5205348137633], [-4288.7666756540975, -2699.735258187162], [-1009.1437950531538, -719.2839240562229], [-832.1393558086985, -650.1385179438951], [-703.6108452839973, -602.003138196753], [-651.5474296666071, -608.8546701757634], [-623.030178566313, -631.3496627033455], [-593.5187974997415, -654.7896605127946], [-573.7778462494696, -689.1585780162067], [-594.5957614957343, -842.2688884265569], [-564.8837421940665, -876.1921066779975], [-531.8731463391997, -906.1934223460464], [-518.5545540239783, -969.5367061132822], [-1597.8078433880564, -3354.39269400701], [-139.72139045401104, -384.9262046803228], [-120.6240791980441, -383.73805390868387], [-100.77601526816373, -381.15253802994783], [-94.07954249009339, -378.7401512710987], [-76.52544471861583, -376.8077843020471], [-49.501687822981985, -373.4837953682543], [-42.698552048601165, -371.30297083776867], [-24.554480098200237, -369.6854440022045], [-10.710005358402684, -367.34390669401745], [3.9496030164579916, -364.97863038267377], [19.77074023999205, -362.71156630353374], [37.0852035764383, -360.0953869125431], [44.08623497681217, -359.0535703562482], [58.877093480216274, -356.41976707713883], [72.91398394653861, -354.58081017596527], [85.71522128742117, -353.763905507117], [101.29721442212383, -351.18246589247343], [118.12900349891184, -347.9968225894513], [131.4419750971524, -346.9351627935104], [148.93277724357947, -342.52193778284385], [163.59285981764774, -341.5976269189871], [180.27797562031907, -338.76823051496376], [194.7571667462272, -337.60140476748944], [212.86417161874894, -334.5162095341028], [231.08546968580183, -331.99360792053267], [249.65668663158544, -329.62674166417133], [259.0676352903685, -328.0365228828287], [279.8273736587626, -324.18313258286474], [299.4621406678793, -322.7111538304979], [309.98557461356484, -320.66328684696356], [332.86756210518354, -316.76400694863014], [354.8851721658146, -314.9710535538784], [381.8808452835917, -309.0203593705267], [408.1949797801657, -307.8204362648298], [437.3335347660079, -302.4854407555328], [469.6372072007981, -295.7463332192551], [504.6763040553815, -291.3750000000003], [542.2403098303425, -286.9803066328666], [2541.8793591106214, -1115.807027996946], [2444.1585337906868, -951.9548632672456], [2492.847314831251, -853.00634665788], [4080.3751295361226, -1225.7238890806827], [4037.106866362367, -1031.2971017460634], [3971.0812433107844, -846.2528118271022], [4206.865460107147, -716.8258160512105], [4195.961465766472, -537.5150605358642], [4198.513592223911, -449.43282691752125], [3496.853336672846, -223.6799986587597], [4121.088774693056, -97.83850775071943]]

def get_distance(point1, point2):
    return math.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)
def neightborFinder(oneTurnPoints)->[]:
    neightborList = []
    distance_limit = 50
    # if distance is less than limit, add to neightborList
    neightbor = []
    for i in range(len(oneTurnPoints)-1):
        point1 = oneTurnPoints[i]
        point2 = oneTurnPoints[i+1]
        distance = get_distance(point1, point2)
        if distance < distance_limit:
            neightbor.append([point1, point2])
        else:
            if len(neightbor) > 0:
                neightborList.append(neightbor)
                neightbor = []
    # remove neightborList with less than 3 points
    neightborList = [neightbor for neightbor in neightborList if len(neightbor) > 3]

    return neightborList

def isLine(neighbors):
    point1 = neighbors[0][0]
    point2 = neighbors[(len(neighbors)//2)][0]
    point3 = neighbors[-1][-1]
    return checkIfParallel(equation_line(point1, point2), equation_line(point1, point3))
def findWall(oneTurn):
    walls = []
    walls_dict_list = []
    neightborList = neightborFinder(oneTurn)
    for neightbor in neightborList:
        if isLine(neightbor):
            print("line")
            point_1 = neightbor[0][0]
            point_2 = neightbor[-1][-1]
            nearest_point = findNearestPoint(point_1, point_2)
            nearest_point_distance = get_distance([0,0],nearest_point)
            print(nearest_point)
            print(nearest_point_distance)
            nearest_point_polar = cartesianToPolar(nearest_point[0], nearest_point[1])
            print(nearest_point_polar)
            wall_dict = {"point_1": point_1, "point_2": point_2, "nearest_point": nearest_point, "nearest_point_distance": nearest_point_distance, "nearest_point_polar": nearest_point_polar}
            wall = [neightbor[0][0], neightbor[-1][-1]]
            print(wall)
            walls.append(wall)
            walls_dict_list.append(wall_dict)
        else:
            print("not line")
    return walls_dict_list

if __name__ == "__main__":
    fig, ax = plt.subplots()
    ax.set_xlim(-1000, 1000)
    ax.set_ylim(-1000, 1000)
    for wall_dict in findWall(oneTurnPoints):
        print(wall_dict)
        # draw the wall
        wall = [wall_dict["point_1"], wall_dict["point_2"]]
        ax.plot([wall[0][0], wall[1][0]], [wall[0][1], wall[1][1]], color='red')
        # draw the nearest point
        nearest_point = wall_dict["nearest_point"]
        ax.scatter(nearest_point[0], nearest_point[1], color="green", s=100)
        # draw the nearest point distance
        ax.text(nearest_point[0]-100, nearest_point[1]-100, str(wall_dict["nearest_point_distance"]), color="green")
        # draw the nearest point polar
        angle = wall_dict["nearest_point_polar"][1]
        ax.text(nearest_point[0]-300, nearest_point[1]-300, str(angle), color="green")

    ax.scatter([point[0] for point in oneTurnPoints], [point[1] for point in oneTurnPoints])
    plt.show()

