from math import cos, sin, atan2, pi, radians
import sympy as sy  #  <--- sympy is a symbolic math library for python https://www.sympy.org/en/index.html
from sympy import (  # <---  run `pip install sympy` in your terminal to install sympy
    Function,
)

# global variable
x, y = sy.symbols("x y")
X, Y = sy.symbols("X Y")
f = Function("f")(x)


def findDistance(point1, point2):
    return ((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2) ** 0.5

def findNearestPoint(line_point_start, line_point_end):
    my_positon = [0, 0]
    # get the point on the line that is closest to the robot

    # get the line equation
    # y = mx + b
    # m = (y2 - y1) / (x2 - x1)
    # b = y - mx
    m = (line_point_end[1] - line_point_start[1]) / (line_point_end[0] - line_point_start[0])
    b = line_point_start[1] - m * line_point_start[0]
    # y = m * x + b
    # get the perpendicular line equation
    # y = mx + b
    # m = -1 / m
    # b = y - mx
    m_perpendicular = -1 / m
    b_perpendicular = my_positon[1] - m_perpendicular * my_positon[0]
    # y = m * x + b
    # get the intersection point
    # y = mx + b
    # y = m * x + b
    # x = (b - b) / (m - m)
    x = (b - b_perpendicular) / (m_perpendicular - m)
    y = m * x + b
    return [x, y]

def findAngle(point1, point2):
    radians = atan2(point2[1] - point1[1], point2[0] - point1[0])
    degrees = abs(radians * 180 / 3.141592653589793)
    return degrees


def equation_line(point1, point2):
    # y = mx + b
    # m = (y2 - y1) / (x2 - x1)
    if point2[0] - point1[0] != 0:
        m = round((point2[1] - point1[1]) / (point2[0] - point1[0]), 0)
        # b = y - mx
        b = round(point1[1] - m * point1[0], 0)
        # y = m * x + b
        #print(f"y = {m} * x + {b}")
        return m * x + b
    # else:
    #     return x - point1[0]


def checkIfParallel(equation_line_1, equation_line_2):
    # y = mx + b
    # m = (y2 - y1) / (x2 - x1)
    # b = y - mx
    # y
    if equation_line_1 == equation_line_2:
        #print(f"equation_line_1: {equation_line_1}\nequation_line_2: {equation_line_2}\nis parallel")
        return True
    else:
        #print(f"equation_line_1: {equation_line_1}\nequation_line_2: {equation_line_2}\nis not parallel")
        return False


def polarToCartesian(degree, distance):
    x = distance * cos(radians(degree))
    y = distance * sin(radians(degree))
    return [x, y]

def cartesianToPolar(x, y):
    distance = (x ** 2 + y ** 2) ** 0.5
    radians = atan2(y, x)
    degrees = abs(radians * 180 / 3.141592653589793)
    return [degrees, distance]