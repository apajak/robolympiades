import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
from time import sleep
import threading
from utils import (
    polarToCartesian,
    findDistance,
    findAngle,
    equation_line,
    checkIfParallel,
)
import math


class ELidar:
    def __init__(self):
        self.oneTurnData = []
        self.oneTurnPoints = []
        self.tenTurnData = []
        self.tenTurnPoints = []
        self.lidarData_Stream = []
        self.detectedWalls = []
        self.stop_scan = False
        self.isConnect = False
        self.motoIsOn = False
        self.message = "No data"

    # port managment
    def connect(self):
        try:
            self.isConnect = True
            print("Port is connected")
        except Exception as e:
            print(f"Port is not connected: {e}")

    def disconnect(self):
        try:
            self.isConnect = False
            print("Port is disconnected")
        except Exception as e:
            print(f"Port is not disconnected: {e}")

    # general managment
    def get_info(self):
        try:
            print(f"Motor is on: [...]")
            print(f"Health: [...]")
            print(f"Info: [...]")
        except Exception as e:
            print(e)

    # motor managment:
    def motor_on(self):
        try:
            self.motor_is_on = True
            print("Motor is on")
        except Exception as e:
            print(e)

    def motor_off(self):
        try:
            self.motor_is_on = False
            print("Motor is off")
        except Exception as e:
            print(e)

    def motor_is_on(self):
        return self.motoIsOn

    def thread_lidar(self):
        # read test.txt file and print the content eache line
        with open("test-2.txt") as f:
            for line in f:  # (False, 19, 17.953125, 606.5)
                line = line.replace("(", "")
                line = line.replace(")", "")
                # split the line into a list
                data = line.split(",")
                boolean = True
                if data[0] == "False":
                    boolean = False

                self.lidarData_Stream = [
                    boolean,
                    int(data[1]),
                    round(float(data[2]), 2),
                    round(float(data[3]), 2),
                ]
                # print(f"data stream: {self.lidarData_Stream}")
                sleep(0.0005)

    def start_EmulatorThread(self):
        emulator_thread = threading.Thread(target=self.thread_lidar, args=())
        emulator_thread.start()

    # mesure managment
    def get_measure(self):
        turnData = []
        turnPoints = []
        while True:
            if self.lidarData_Stream:
                mesure = self.lidarData_Stream
                new_scan = mesure[0]
                quality = mesure[1]
                angle = mesure[2]
                distance = mesure[3]
                if quality > 0 and distance > 0:
                    if len(turnData) == 0 or angle > turnData[-1][2]:
                        turnData.append(mesure)
                        turnPoints.append(polarToCartesian(angle, distance))
                    else:
                        self.oneTurnData = turnData
                        # print(f"one turn data: {self.oneTurnData}\n")
                        self.oneTurnPoints = turnPoints
                        turnData = []
                        turnPoints = []
                        turnData.append(mesure)
                        turnPoints.append(polarToCartesian(angle, distance))
                if self.stop_scan:
                    break
            sleep(0.001)

    def start_scan(self):
        self.stop_scan = False
        scan_thread = threading.Thread(target=self.get_measure, args=())
        scan_thread.start()

    def clear(self):
        print("clear")

    def distanceLimit(self, data, limit):
        return [mesure for mesure in data if mesure[3] < limit]
    def get_distance(self,point1, point2):
        return math.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)

    def neightborFinder(self,oneTurnPoints)->[]:
        neightborList = []
        distance_limit = 50
        # if distance is less than limit, add to neightborList
        neightbor = []
        for i in range(len(oneTurnPoints)-1):
            point1 = oneTurnPoints[i]
            point2 = oneTurnPoints[i+1]
            distance = self.get_distance(point1, point2)
            if distance < distance_limit:
                neightbor.append([point1, point2])
            else:
                if len(neightbor) > 0:
                    neightborList.append(neightbor)
                    neightbor = []
        # remove neightborList with less than 3 points
        neightborList = [neightbor for neightbor in neightborList if len(neightbor) > 3]

        return neightborList

    def isLine(self,neighbors):
        point1 = neighbors[0][0]
        point2 = neighbors[(len(neighbors)//2)][0]
        point3 = neighbors[-1][-1]
        return checkIfParallel(equation_line(point1, point2), equation_line(point1, point3))

    def findWalls(self,detection_range):
        while True:
            walls = []
            neightborList = self.neightborFinder(self.oneTurnPoints)
            for neightbor in neightborList:
                if self.isLine(neightbor):
                    wall = [neightbor[0][0], neightbor[-1][-1]]
                    walls.append(wall)
            self.detectedWalls = walls
            sleep(0.1)

    def start_wallDetection(self, detection_range):
        findWalls_thread = threading.Thread(
            target=self.findWalls, args=(detection_range,)
        )
        findWalls_thread.start()
