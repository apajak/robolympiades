from rplidar import RPLidar
if __name__ == '__main__':
    lidar = RPLidar("/dev/cu.SLAB_USBtoUART")

    info = lidar.get_info()
    print(info)

    health = lidar.get_health()
    print(health)

    for i, scan in enumerate(lidar.iter_scans()):
        print('%d: Got %d measurments' % (i, len(scan)))
        for (_, angle, distance) in scan:
            print('angle: %f dist: %f' % (angle, distance))
    lidar.stop()
    lidar.stop_motor()
    lidar.disconnect()