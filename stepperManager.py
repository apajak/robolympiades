#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2023/01/04
# version ='1.0'
# ---------------------------------------------------------------------------
""" NEMA 17 (17HS4023) Raspberry Pi Manager"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import RPi.GPIO as GPIO
from RpiMotorLib import RpiMotorLib
import time

################################
# RPi and Motor Pre-allocations
################################
#
# define GPIO pins for motor1
direction = 23  # Direction (DIR) GPIO Pin
step = 22  # Step GPIO Pin
EN_pin = 24  # enable pin (LOW to enable)
# define GPIO pins for motor2
direction2 = 24  # Direction (DIR) GPIO Pin
step2 = 25  # Step GPIO Pin

# Declare a instance of class pass GPIO pins numbers and the motor type
mymotortest = RpiMotorLib.A4988Nema(direction, step, (21, 21, 21), "DRV8825")
GPIO.setup(EN_pin, GPIO.OUT)  # set enable pin as outputs

###########################
# Actual motor control
###########################
#
GPIO.output(EN_pin, GPIO.LOW)  # pull enable to low to enable motor
# time start
start = time.time()
mymotortest.motor_go(True, "Full", 200, 0.005, False, 0.05)
# time end
end = time.time()
# total time taken
print(f"Runtime of the program is {end - start}")
GPIO.output(EN_pin, GPIO.HIGH)  # pull enable to high to disable motor
# mymotortest.motor_go(True, 'Full', 200, .005, False, .05)
