#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: Pajak Alexandre
# date: 2022-12-07
# version: 0.1
# description: Test I2C communication with ESP32
# ----------------------------------------------------------------------------
# Imports
# ----------------------------------------------------------------------------
import smbus
import time
import sys

# Slave Addresses for Arduinos
ESP32_1_ADDRESS = 0x04  # I2C Address of ESP32 1

# Create the I2C bus
I2Cbus = smbus.SMBus(1)

message = "Hello from Raspberry Pi"
BytesToSend = message.encode("utf-8")
I2Cbus.write_i2c_block_data(ESP32_1_ADDRESS, 0x00, BytesToSend)
print(f"Sent {message} to ESP32 1 {ESP32_1_ADDRESS}")
