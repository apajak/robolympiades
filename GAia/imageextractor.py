#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2023/02/15
# version ='1.0'
# ---------------------------------------------------------------------------
""" Python image extractor for IA training"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import os
import sys
import argparse
import cv2

# ---------------------------------------------------------------------------
# Main
# ---------------------------------------------------------------------------
def main():
    # read video file
    cap = cv2.VideoCapture("IA_video.mp4")
    # check if video file is opened
    if not cap.isOpened():
        print("Error opening video stream or file")
        sys.exit(1)
    image_counter = 1
    image_name = "train_image_"
    while cap.isOpened():
        # read frame
        ret, frame = cap.read()
        if ret:
            # save frame as JPEG file in images folder
            cv2.imwrite("images/" + image_name + str(image_counter) + ".jpg", frame)
            image_counter += 1
        else:
            print("Empty frame")
            pass
    # release video file
    print("Video file closed")
    cap.release()

if __name__ == "__main__":
    print("Python image extractor for IA training")
    main()
    print("End of program")

