imagesPath="./"
testPath="./test/"
trainPath="./train/"

# get all .jpg files
jpgFiles=$(find $imagesPath -name "*.jpg")

# set index to 0
index=0

# loop over jpg files
for jpgFile in $jpgFiles
# if modulus index 10 is 0 move jpg and xml files to test folder
# else move jpg and xml files to train folder
do
    if [ $(($index % 10)) -eq 0 ]
    then
        mv $jpgFile $testPath
    else
        mv $jpgFile $trainPath
    fi
    index=$(($index + 1))
done