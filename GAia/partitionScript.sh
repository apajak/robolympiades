#!/bin/bash
# author: 	Pajak Alexandre
# date: 	2023-02-15
# This script will partition .xml files in a folder train/test with a ratio of 90/10

# global variables
imagesPath="../workspace/training_test/images"
testPath="../workspace/training_test/images/test"
trainPath="../workspace/training_test/images/train"
ratio=0.9

# get all xml files
xmlFiles=$(find $imagesPath -name "*.xml")

# loop over xml files
for xmlFile in $xmlFiles
# with a ratio of 90/10 for train/test folders move xml and jpg files to the right folder
do
    if [ $(echo "$RANDOM / 32767.0 < $ratio" | bc) -eq 1 ]
    then
        mv $xmlFile $trainPath
        mv ${xmlFile%.*}.jpg $trainPath
    else
        mv $xmlFile $testPath
        mv ${xmlFile%.*}.jpg $testPath
    fi
done

echo "Done"
