#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: Pajak Alexandre
# date: 2022-11-23
# version: 0.1
# description: Lidar detection
# import the necessary packages
from rplidar import RPLidar
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
from time import sleep
import threading

# global variables
PORT_NAME = "/dev/cu.SLAB_USBtoUART"
stop_scan = False
scan_data = []
message = "No data"


class Lidar:
    def __init__(self):
        self.lidar = RPLidar(PORT_NAME)

    # port managment
    def connect(self):
        try:
            self.lidar.connect()
            print("Port is connected")
        except Exception as e:
            print(f"Port is not connected: {e}")

    def disconnect(self):
        try:
            self.lidar.disconnect()
            print("Port is disconnected")
        except Exception as e:
            print(f"Port is not disconnected: {e}")

    # general managment
    def get_info(self):
        try:
            print(f"Motor is on: {self.lidar.motor_running}")
            print(f"Health: {self.lidar.get_health()}")
            print(f"Info: {self.lidar.get_info()}")
        except Exception as e:
            print(e)

    # motor managment:
    def motor_on(self):
        try:
            self.lidar.start_motor()
            print("Motor is on")
        except Exception as e:
            print(e)

    def motor_off(self):
        try:
            self.lidar.stop_motor()
            print("Motor is off")
        except Exception as e:
            print(e)

    def motor_is_on(self):
        print(self.lidar.motor_running)
        print(self.lidar._motor_speed)
        return self.lidar.motor_running

    # mesure managment
    def get_mesure(self):
        global scan_data
        global message
        scanTab = []
        result = []
        for i, scan in enumerate(self.lidar.iter_scans()):
            scan_data = scan
            scanTab.append(scan)
            #print(scan)
            if i > 10:
                # foreach scan in scanTab
                for scan in scanTab:
                    # foreach mesure in scan
                    for mesure in scan:
                        # if distance < 15 cm
                        if mesure[2] < 150:
                            print("Object detected at: " + str(mesure[2]) + " cm and at " + str(mesure[1]) + "°")
                break

    def start_mesure(self):
        try:
            while True:
                tab = self.get_mesure()
                print(tab)
                sleep(0.5)
                if stop_scan:
                    break
        except Exception as e:
            print(e)

    def stop_mesure(self):
        print("Stop mesure")
        global stop_scan
        stop_scan = True

    def clear(self):
        self.lidar.reset()


# main
if __name__ == "__main__":
    print("Welcome to the lidar test program")
    lidar = Lidar()
    lidar.connect()
    lidar.get_info()
    lidar.motor_on()
    lidar.get_mesure()

    # while True:
    #     # get user input
    #     user_input = input("Enter a command: ")
    #     try:
    #         if user_input == "info":
    #             lidar.get_info()
    #         elif user_input == "motor on":
    #             lidar.motor_on()
    #         elif user_input == "motor off":
    #             lidar.motor_off()
    #         elif user_input == "mesure":
    #             try:
    #                 print(lidar.get_mesure())
    #             except Exception as e:
    #                 print(e)
    #         elif user_input == "mesure start":
    #             try:
    #                 thread_mesure = threading.Thread(target=lidar.start_mesure())
    #                 thread_mesure.start()
    #             except Exception as e:
    #                 print(e)
    #         elif user_input == "mesure stop":
    #             try:
    #                 lidar.stop_mesure()
    #             except Exception as e:
    #                 print(e)

    #         elif user_input == "exit":
    #             lidar.motor_off()
    #             sleep(1)
    #             lidar.disconnect()
    #             print("Bye")
    #             break
    #     # if user interrupt program
    #     except KeyboardInterrupt:
    #         lidar.motor_off()
    #         sleep(1)
    #         lidar.disconnect()
    #         print("Bye")
    #         break
