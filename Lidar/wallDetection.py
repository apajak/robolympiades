#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: Pajak Alexandre
# date: 2023-02-22
# version: 0.1
# description: Lidar wall detection

import threading
from time import sleep
from math import cos, sin, atan2, pi, radians
from fractions import Fraction
import sympy as sy  #  <--- sympy is a symbolic math library for python https://www.sympy.org/en/index.html
from sympy import (  # <---  run `pip install sympy` in your terminal to install sympy
    Function,
)
from matplotlib import pyplot as plt

# global variable
lidarData_Stream = []
lidar_run = True
previusMesure = [False, 1, 0.0, 0.1]
lidartmp = []

x, y = sy.symbols("x y")
X, Y = sy.symbols("X Y")
f = Function("f")(x)


def polarToCartesian(angle, distance):
    rad = radians(angle)
    x = distance * cos(rad)
    y = distance * sin(rad)
    return [x, y]


def findDistance(point1, point2):
    return ((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2) ** 0.5


def findAngle(point1, point2):
    radians = atan2(point2[1] - point1[1], point2[0] - point1[0])
    degrees = abs(radians * 180 / 3.141592653589793)
    return degrees


def equation_line(point1, point2):
    # y = mx + b
    # m = (y2 - y1) / (x2 - x1)
    if point2[0] - point1[0] != 0:
        m = round((point2[1] - point1[1]) / (point2[0] - point1[0]), 0)
        # b = y - mx
        b = round(point1[1] - m * point1[0], 0)
        # y = m * x + b
        return m * x + b
    # else:
    #     return x - point1[0]


def checkIfParallel(equation_line_1, equation_line_2):
    # y = mx + b
    # m = (y2 - y1) / (x2 - x1)
    # b = y - mx
    # y
    if equation_line_1 == equation_line_2:
        return True
    else:
        return False


def thread_lidar():
    # global variable
    global lidarData_Stream, lidar_run
    # read test.txt file and print the content eache line
    while lidar_run:
        with open("test-2.txt") as f:
            print("------------$$$$$$$$$ new tour $$$$$$$$$$$------------")
            for line in f:  # (False, 19, 17.953125, 606.5)
                line = line.replace("(", "")
                line = line.replace(")", "")
                # split the line into a list
                data = line.split(",")
                boolean = True
                if data[0] == "False":
                    boolean = False

                lidarData_Stream = [
                    boolean,
                    int(data[1]),
                    round(float(data[2]), 2),
                    round(float(data[3]), 2),
                ]
                # print(lidarData_Stream)
                sleep(0.01)


def findParallelLines(data):
    detectedWalls = []
    for i in range(len(data) - 2):
        mesure_1 = data[i]
        mesure_2 = data[i + 1]
        mesure_3 = data[i + 2]
        point1 = polarToCartesian(mesure_1[2], mesure_1[3])
        point2 = polarToCartesian(mesure_2[2], mesure_2[3])
        point3 = polarToCartesian(mesure_3[2], mesure_3[3])
        if point1 != point2 and point1 != point3 and point2 != point3:
            equation_line_1 = equation_line(point1, point2)
            equation_line_2 = equation_line(point1, point3)
            if checkIfParallel(equation_line_1, equation_line_2):
                print("is parallel: True")
                isParallel = True
                index = i + 3
                line = [mesure_1, mesure_3]
                while isParallel:
                    index += 1
                    if index < len(data) - 1:
                        mesure_n = data[index]
                        point_n = polarToCartesian(mesure_n[2], mesure_n[3])
                        equation_line_2 = equation_line(point1, point_n)
                        if checkIfParallel(equation_line_1, equation_line_2):
                            line.append(mesure_n)
                        if not checkIfParallel(equation_line_1, equation_line_2):
                            isParallel = False
                    else:
                        isParallel = False

                # wall data creation
                nearestMesure = None
                for mesure in line:
                    if nearestMesure == None:
                        nearestMesure = mesure
                    elif nearestMesure[3] > mesure[3]:
                        nearestMesure = mesure
                    else:
                        pass

                line_start = line[0]
                line_end = line[-1]
                point_start = polarToCartesian(line_start[2], line_start[3])
                point_end = polarToCartesian(line_end[2], line_end[3])
                lenght = findDistance(point_start, point_end)
                wall_angle = findAngle(point_start, point_end)
                # TODO add wall class for storing and processing data
                wall = [
                    line_start,
                    line_end,
                    point_start,
                    point_end,
                    lenght,
                    wall_angle,
                    nearestMesure,
                ]
                print("--------WALL DETECTED---------")
                print(f"wall lenght: {round(lenght/10,2)}cm")
                print(f"wall angle: {round(wall_angle,2)}°")
                print(f"wall nearest distance: {round(nearestMesure[3]/10,2)}cm")
                print(f"wall nearest angle: {round(nearestMesure[2],2)}°")
                print("------------------------------")
                print("wall start point: ", point_start)
                print("wall end point: ", point_end)
                print("------------------------------")
                print("wall start mesure: ", line_start)
                print("wall end mesure: ", line_end)
                print("------------------------------")

                detectedWalls.append(wall)
    return detectedWalls


def main():
    # diplay the window
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlim(-500, 500)
    ax.set_ylim(-500, 500)
    # ax.set_aspect("equal")
    ax.grid()
    ax.set_title("Lidar")
    ax.set_xlabel("x")
    ax.set_ylabel("y")

    # create thread
    thread = threading.Thread(target=thread_lidar)
    # start thread
    thread.start()
    # global variable
    global lidarData_Stream, lidar_run, previusMesure, lidartmp
    turnMesures = []
    oneTurnData = []

    while True:
        sleep(0.01)
        turnCounter = 0
        accuracy = lidarData_Stream[1]
        distance = lidarData_Stream[3]
        if distance == 0.0 or accuracy == 0:
            pass
        else:
            boolean = lidarData_Stream[0]
            angle = lidarData_Stream[2]
            distance = lidarData_Stream[3]
            if len(oneTurnData) == 0 or angle > oneTurnData[-1][2]:
                oneTurnData.append(lidarData_Stream)
            else:
                print("one turn")
                parallel_lines = findParallelLines(oneTurnData)
                oneTurnData = []
                turnCounter += 1
                # clear the plot
                ax.clear()
                for point in oneTurnData:
                    # draw a line between the two points
                    ax.plot(point[2], point[3], "ro")
                # clear the plot
                plt.pause(0.001)
                print("turnCounter: ", turnCounter)


if __name__ == "__main__":
    main()
