from rplidar import RPLidar
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
from time import sleep
import threading

# global variables
PORT_NAME = "/dev/cu.SLAB_USBtoUART"
stop_scan = False
scan_data = []
message = "No data"



class Lidar:
    def __init__(self):
        self.lidar = RPLidar(PORT_NAME)

    # port managment
    def connect(self):
        try:
            self.lidar.connect()
            print("Port is connected")
        except Exception as e:
            print(f"Port is not connected: {e}")

    def disconnect(self):
        try:
            self.lidar.disconnect()
            print("Port is disconnected")
        except Exception as e:
            print(f"Port is not disconnected: {e}")

    # general managment
    def get_info(self):
        try:
            print(f"Motor is on: {self.lidar.motor_running}")
            print(f"Health: {self.lidar.get_health()}")
            print(f"Info: {self.lidar.get_info()}")
        except Exception as e:
            print(e)

    # motor managment:
    def motor_on(self):
        try:
            self.lidar.start_motor()
            print("Motor is on")
        except Exception as e:
            print(e)

    def motor_off(self):
        try:
            self.lidar.stop_motor()
            print("Motor is off")
        except Exception as e:
            print(e)

    def motor_is_on(self):
        print(self.lidar.motor_running)
        print(self.lidar._motor_speed)
        return self.lidar.motor_running

    # mesure managment
    def get_mesure(self):
        global scan_data
        global message
        for _, scan in enumerate(self.lidar.iter_scans()):
            min_distance = 10000
            scan_data = scan
            for (_, angle, distance) in scan:
                if distance < min_distance:
                    min_distance = distance
                    print(f"nearest object: {min_distance} mm. at {angle}°")

    def start_mesure(self):
        try:
            while True:
                tab = self.get_mesure()
                print(tab)
                sleep(0.5)
                if stop_scan:
                    break
        except Exception as e:
            print(e)

    def stop_mesure(self):
        print("Stop mesure")
        global stop_scan
        stop_scan = True

    def clear(self):
        self.lidar.reset()


# main
if __name__ == "__main__":
    print("Welcome to the lidar test program")
    lidar = Lidar()
    lidar.connect()
    # change the motor speed
    lidar.get_info()
    lidar.motor_on()
    # create a thread to get the mesure
    thread = threading.Thread(target=lidar.get_mesure)
    # create a thread to draw the lidar
    # start the thread
    thread.start()
