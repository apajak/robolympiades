import threading
from time import sleep
from math import cos, sin, atan2
from fractions import Fraction
import sympy as sy
from sympy import Function


def polarToCartesian(angle, distance):
    x = distance * cos(angle)
    y = distance * sin(angle)
    return [x, y]


def findDistance(point1, point2):
    return ((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2) ** 0.5


def findAngle(point1, point2):
    return atan2(point2[1] - point1[1], point2[0] - point1[0])


x, y = sy.symbols("x y")
X, Y = sy.symbols("X Y")
f = Function("f")(x)


def equation_line(point1, point2):
    # y = mx + b
    # m = (y2 - y1) / (x2 - x1)
    if point2[0] - point1[0] != 0:
        m = round((point2[1] - point1[1]) / (point2[0] - point1[0]), 2)
        # b = y - mx
        b = round(point1[1] - m * point1[0], 2)
        # y = m * x + b
        return m * x + b
    else:
        return x - point1[0]


def checkIfParallel(equation_line_1, equation_line_2):
    # y = mx + b
    # m = (y2 - y1) / (x2 - x1)
    # b = y - mx
    # y
    if equation_line_1 == equation_line_2:
        return True
    else:
        return False
def getDataset():
        dataset = []
        with open("oneTour.txt") as f:
            for line in f:
                line = line.replace(" ", "")
                line = line.split("],")
                for i in range(len(line)):
                    line[i] = line[i].replace("[", "")
                    line[i] = line[i].replace("]", "")
                    line[i] = line[i].replace(" ", "")
                    line[i] = line[i].split(",")
                    boolean = True
                    if line[i][0] == "False":
                        boolean = False
                    accuracy = int(line[i][1])
                    angle = float(line[i][2])
                    distance = float(line[i][3])
                    dataset.append([boolean, accuracy, angle, distance])
        return dataset
def findParallelLines(data):
    mesurecounter = 0
    linearEquation_tab = []
    for i in range(len(data)-2):
        mesure_1 = data[i]
        mesure_2 = data[i+1]
        mesure_3 = data[i+2]
        if mesure_1[1] != 0 and mesure_2[1] != 0 and mesure_3[1] != 0:
            mesurecounter += 1
            point1 = polarToCartesian(mesure_1[2], mesure_1[3])
            point2 = polarToCartesian(mesure_2[2], mesure_2[3])
            point3 = polarToCartesian(mesure_3[2], mesure_3[3])
            if point1 != point2 and point1 != point3 and point2 != point3:
                #print(f"cartesian -> point1: {point1}, point2: {point2}, point3: {point3}")
                equation_line_1 = equation_line(point1, point2)
                equation_line_2 = equation_line(point1, point3)
                #print(f"equation_line -> equation_line_1: {equation_line_1}, equation_line_2: {equation_line_2}")
                if checkIfParallel(equation_line_1, equation_line_2):
                    print("parallel")
                    print(f"cartesian -> point1: {point1}, point2: {point2}, point3: {point3}")
                    print(f"polars -> mesure_1: {mesure_1}, mesure_2: {mesure_2}, mesure_3: {mesure_3}")
                    print(f"equation_line -> equation_line_1: {equation_line_1}, equation_line_2: {equation_line_2}")
                    linearEquation_tab.append(equation_line_1)
                else:
                    pass
    if len(linearEquation_tab) > 0:
        print(f"linearEquation_tab: {linearEquation_tab}")
    else:
        print("no parallel lines found")        

def main():
    data = getDataset()
    findParallelLines(data)

if __name__ == "__main__":
    main()



                