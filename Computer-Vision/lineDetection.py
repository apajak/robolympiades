#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2022/12/07
# version ='1.0'
# ---------------------------------------------------------------------------
""" Python wall detection"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import cv2
import numpy as np

# Read the camera image
cap = cv2.VideoCapture(0)

while True:
    success, img = cap.read()
    # Convert the img to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Apply edge detection method on the image
    edges = cv2.Canny(gray, 50, 150, apertureSize=3)

    # This returns an array of r and theta values
    lines = cv2.HoughLines(edges, 1, np.pi / 180, 200)
    if lines is not None:
        for line in lines:
            r, theta = line[0]
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a * r
            y0 = b * r
            x1 = int(x0 + 1000 * (-b))
            y1 = int(y0 + 1000 * (a))
            x2 = int(x0 - 1000 * (-b))
            y2 = int(y0 - 1000 * (a))
            cv2.line(img, (x1, y1), (x2, y2), (0, 0, 255), 2)
            print("r = ", r, "theta = ", theta)
            print("x1 = ", x1, "y1 = ", y1, "x2 = ", x2, "y2 = ", y2)
        # dislpay the image
    cv2.imshow("Image", img)
    cv2.waitKey(1)
