#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2023/01/18
# version ='2.0'
# ---------------------------------------------------------------------------
""" Python Color detection """
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import numpy as np
import cv2

minimal_aera = 1000
maximal_aera = 10000
blue = [214, 145, 24]
purple = [129, 0, 74]
orange = [13, 113, 254]
red = [21, 0, 208]
tolerance = 3


# Capturing video through webcam
webcam = cv2.VideoCapture(0)
# Start a while loop
while True:
    # Reading the video from the
    # webcam in image frames
    _, imageFrame = webcam.read()
    # Convert the imageFrame in
    # BGR(RGB color space) to
    # HSV(hue-saturation-value)
    # color space
    hsvFrame = cv2.cvtColor(imageFrame, cv2.COLOR_BGR2HSV)

    # Set range for red color and
    # define mask

    red_lower = np.array([136, 87, 111], np.uint8)
    red_upper = np.array([180, 255, 255], np.uint8)
    red_mask = cv2.inRange(hsvFrame, red_lower, red_upper)

    # Set range for orange color and
    # define mask
    orange_lower = np.array([5, 50, 50], np.uint8)
    orange_upper = np.array([15, 255, 255], np.uint8)
    orange_mask = cv2.inRange(hsvFrame, orange_lower, orange_upper)

    # Set range for blue color and
    # define mask
    blue_lower = np.array([94, 80, 2], np.uint8)
    blue_upper = np.array([120, 255, 255], np.uint8)
    blue_mask = cv2.inRange(hsvFrame, blue_lower, blue_upper)

    # Set range for purple color and
    # define mask
    purple_lower = np.array([125, 50, 50], np.uint8)
    purple_upper = np.array([145, 255, 255], np.uint8)
    purple_mask = cv2.inRange(hsvFrame, purple_lower, purple_upper)

    # Morphological Transform, Dilation
    # for each color and bitwise_and operator
    # between imageFrame and mask determines
    # to detect only that particular color
    kernal = np.ones((5, 5), "uint8")

    # For red color
    red_mask = cv2.dilate(red_mask, kernal)
    res_red = cv2.bitwise_and(imageFrame, imageFrame, mask=red_mask)

    # For orange color
    orange_mask = cv2.dilate(orange_mask, kernal)
    res_orange = cv2.bitwise_and(imageFrame, imageFrame, mask=orange_mask)

    # For blue color
    blue_mask = cv2.dilate(blue_mask, kernal)
    res_blue = cv2.bitwise_and(imageFrame, imageFrame, mask=blue_mask)

    # For purple color
    purple_mask = cv2.dilate(purple_mask, kernal)
    res_purple = cv2.bitwise_and(imageFrame, imageFrame, mask=purple_mask)

    # Creating contour to track red color
    contours, hierarchy = cv2.findContours(
        red_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
    )

    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if minimal_aera < area < maximal_aera:
            x, y, w, h = cv2.boundingRect(contour)
            imageFrame = cv2.rectangle(
                imageFrame, (x, y), (x + w, y + h), (0, 0, 255), 2
            )

            cv2.putText(
                imageFrame,
                "Red Colour",
                (x, y),
                cv2.FONT_HERSHEY_SIMPLEX,
                1.0,
                (0, 0, 255),
            )

    # Creating contour to track orange color
    contours, hierarchy = cv2.findContours(
        orange_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
    )
    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if minimal_aera < area < maximal_aera:
            x, y, w, h = cv2.boundingRect(contour)
            imageFrame = cv2.rectangle(
                imageFrame, (x, y), (x + w, y + h), (0, 140, 255), 2
            )

            cv2.putText(
                imageFrame,
                "Orange Colour",
                (x, y),
                cv2.FONT_HERSHEY_SIMPLEX,
                1.0,
                (0, 140, 255),
            )

    # Creating contour to track blue color
    contours, hierarchy = cv2.findContours(
        blue_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
    )
    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if minimal_aera < area < maximal_aera:
            x, y, w, h = cv2.boundingRect(contour)
            imageFrame = cv2.rectangle(
                imageFrame, (x, y), (x + w, y + h), (255, 0, 0), 2
            )

            cv2.putText(
                imageFrame,
                "Blue Colour",
                (x, y),
                cv2.FONT_HERSHEY_SIMPLEX,
                1.0,
                (255, 0, 0),
            )

    # Creating contour to track purple color
    contours, hierarchy = cv2.findContours(
        purple_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
    )
    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if minimal_aera < area < maximal_aera:
            x, y, w, h = cv2.boundingRect(contour)
            imageFrame = cv2.rectangle(
                imageFrame, (x, y), (x + w, y + h), (255, 0, 255), 2
            )

            cv2.putText(
                imageFrame,
                "Purple Colour",
                (x, y),
                cv2.FONT_HERSHEY_SIMPLEX,
                1.0,
                (255, 0, 255),
            )

    # Program Termination
    cv2.imshow("Multiple Color Detection in Real-TIme", imageFrame)
    if cv2.waitKey(10) & 0xFF == ord("q"):
        webcam.release()
        cv2.destroyAllWindows()
        break
