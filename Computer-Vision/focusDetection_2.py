#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2023/01/31
# version ='2.0'
# ---------------------------------------------------------------------------
""" Python Focus detection for puck height detection """

# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import cv2
import numpy as np

# open image and convert to grayscale
img = cv2.imread("img_test_2.jpg")
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# keep only sharp pixels
mask = cv2.Laplacian(gray, cv2.CV_8U, ksize=5)
mask = cv2.threshold(mask, 100, 255, cv2.THRESH_BINARY)[1]

# isolate pixels get the neighbor color
mask = cv2.dilate(mask, None)
mask = cv2.erode(mask, None)

# apply morphology to remove isolated extraneous noise
# use borderconstant of black since foreground touches the edges
kernel = np.ones((3,3), np.uint8)
mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

# anti-alias the mask -- blur then stretch
# blur alpha channel
mask = cv2.GaussianBlur(mask, (0,0), sigmaX=2, sigmaY=2, borderType = cv2.BORDER_DEFAULT)

contours, hierarchy = cv2.findContours(
    mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
)
# keep only the biggest contour
contours = sorted(contours, key=cv2.contourArea, reverse=True)[:1]
# draw contours
cv2.drawContours(img, contours, -1, (0, 255, 0), 3)
    # drow a bounding box
for cnt in contours:
    x, y, w, h = cv2.boundingRect(cnt)
    if w > 100 and h > 100:
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
        # write the height of the bounding box
        cv2.putText(
            img, str(h), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2
        )
# display image
cv2.imshow("Original", img)
cv2.waitKey(0)


