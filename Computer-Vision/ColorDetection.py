#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2023/01/18
# version ='1.0'
# ---------------------------------------------------------------------------
""" Python Color detection """
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------


import cv2

import numpy as np
import random as rng

# blue color base value : R: 24, G: 145, B: 214
blue = np.array([0, 0, 200])
# purple color base value : R: 74, G: 0, B: 129
purple = np.array([129, 0, 74])
# orange color base value : R: 254, G: 113, B: 13
orange = np.array([13, 113, 254])
# red color base value : R: 208, G: 0, B: 21
red = np.array([21, 0, 208])

red_color = True
blue_color = True
orange_color = True
purple_color = True

# red color range:
lower_red = np.array([20, 0, 208], dtype="uint8")
upper_red = np.array([25, 0, 255], dtype="uint8")

# blue color range:
lower_blue = np.array([200, 140, 0], dtype="uint8")
upper_blue = np.array([255, 150, 30], dtype="uint8")

# orange color range:"
lower_orange = np.array([0, 100, 200], dtype="uint8")
upper_orange = np.array([20, 255, 255], dtype="uint8")

# purple color range:
lower_purple = np.array([100, 0, 70], dtype="uint8")
upper_purple = np.array([200, 0, 100], dtype="uint8")

image = cv2.imread("test.jpeg")


def draw_contours(image, mask, detected_color) -> image:
    # convert the image to grayscale
    img_gray = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
    # detect the contours on the binary image using cv2.CHAIN_APPROX_NONE
    contours, hierarchy = cv2.findContours(
        image=img_gray, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_NONE
    )
    # draw contours on the original image
    # Approximate contours to polygons + get bounding rects and circles
    contours_poly = [None] * len(contours)
    boundRect = [None] * len(contours)
    centers = [None] * len(contours)
    radius = [None] * len(contours)
    for i, c in enumerate(contours):
        contours_poly[i] = cv2.approxPolyDP(c, 3, True)
        boundRect[i] = cv2.boundingRect(contours_poly[i])
        centers[i], radius[i] = cv2.minEnclosingCircle(contours_poly[i])
    # Draw polygonal contour + bonding rects + circles
    for i in range(len(contours)):
        color = (0, 0, 255)
        thin = 5
        police = 5
        cv2.drawContours(image, contours_poly, i, color)
        cv2.rectangle(
            image,
            (int(boundRect[i][0]), int(boundRect[i][1])),
            (
                int(boundRect[i][0] + boundRect[i][2]),
                int(boundRect[i][1] + boundRect[i][3]),
            ),
            color,
            thin,
        )
        cv2.circle(
            image, (int(centers[i][0]), int(centers[i][1])), int(radius[i]), color, 10
        )
        # add color text
        cv2.putText(
            image,
            detected_color,
            (int(centers[i][0] - 200), int(centers[i][1])),
            cv2.FONT_HERSHEY_SIMPLEX,
            police,
            color,
            thin,
        )
        # add coordinates text
        cv2.putText(
            image,
            "x: " + str(int(centers[i][0])) + " y: " + str(int(centers[i][1])),
            (int(centers[i][0] - 600), int(centers[i][1] + 150)),
            cv2.FONT_HERSHEY_SIMPLEX,
            police,
            color,
            thin,
        )
    return image


# cap = cv2.VideoCapture(0)
while True:
    # _, image = cap.read()
    image = cv2.imread("test_2.png")
    if red_color:
        mask_red = cv2.inRange(image, lower_red, upper_red)
        detected_output_red = cv2.bitwise_and(image, image, mask=mask_red)
        image = draw_contours(image, detected_output_red, "red")
    if blue_color:
        mask_blue = cv2.inRange(image, lower_blue, upper_blue)
        detected_output_blue = cv2.bitwise_and(image, image, mask=mask_blue)
        # detect the contours on the binary image using cv2.CHAIN_APPROX_NONE
        image = draw_contours(image, detected_output_blue, "blue")
    if orange_color:
        mask_orange = cv2.inRange(image, lower_orange, upper_orange)
        detected_output_orange = cv2.bitwise_and(image, image, mask=mask_orange)
        image = draw_contours(image, detected_output_orange, "orange")
    if purple_color:
        mask_purple = cv2.inRange(image, lower_purple, upper_purple)
        detected_output_purple = cv2.bitwise_and(image, image, mask=mask_purple)
        image = draw_contours(image, detected_output_purple, "purple")

    # Displaying the frame
    cv2.imshow("Color Detection", image)
    if cv2.waitKey(10) & 0xFF == ord("q"):
        image.release()
        cv2.destroyAllWindows()
        break
