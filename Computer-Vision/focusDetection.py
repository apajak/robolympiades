#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2023/01/31
# version ='1.0'
# ---------------------------------------------------------------------------
""" Python Focus detection for puck height detection """

# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import cv2
import cvzone
from cvzone.SelfiSegmentationModule import SelfiSegmentation
import numpy as np

segmentor = SelfiSegmentation()

# open camera
cap = cv2.VideoCapture(0)

while True:
    # read image
    ret, img = cap.read()
    # get the image width and height
    height, width, channels = img.shape
    black = (0, 0, 0)
    white = (255, 255, 255)
    threshold = 0.50
    imgNoBg = segmentor.removeBG(img, white, threshold=0.50)

    # drow contour
    img_gray = cv2.cvtColor(imgNoBg, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(img_gray, 127, 255, 0)

    contours, hierarchy = cv2.findContours(
        thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
    )
    # # remove the contour if widht or height is same as the image
    # contours = [
    #     cnt
    #     for cnt in contours
    #     if cv2.boundingRect(cnt)[2] < width - 10
    #     and cv2.boundingRect(cnt)[3] < height - 10
    # ]

    cv2.drawContours(img, contours, -1, (0, 255, 0), 3)

    # drow a bounding box
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        if w > 100 and h > 100:
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
            # write the height of the bounding box
            cv2.putText(
                img, str(h), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2
            )

    # show both images
    #horizontal = np.hstack((img, imgNoBg))
    cv2.imshow("focus_Exclusion", imgNoBg)
    # add cursor to the image for treshold
    #cvzone.cornerRect(imgNoBg, [0, 0, 0, 0], 20, rt=0)

    if cv2.waitKey(1) & 0xFF == ord("q"):
        break

# close camera
cap.release()
cv2.destroyAllWindows()
