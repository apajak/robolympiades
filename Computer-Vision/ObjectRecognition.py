#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2022/12/07
# version ='1.0'
# ---------------------------------------------------------------------------
""" Python Circle detection and tracking"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------

import cv2
import numpy as np

# capturing video through webcam

cap = cv2.VideoCapture(0)

while 1:
    _, img = cap.read()

    # converting frame(img == BGR) to HSV(hue-saturation-value)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    # converting to gray scale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # converting to GaussianBlur
    blurred = cv2.medianBlur(gray, 25)

    # red color
    red_lower = np.array([136, 87, 111], np.uint8)
    red_upper = np.array([180, 255, 255], np.uint8)
    # blue color
    blue_lower = np.array([99, 115, 150], np.uint8)
    blue_upper = np.array([110, 255, 255], np.uint8)
    # yellow color
    yellow_lower = np.array([22, 60, 200], np.uint8)
    yellow_upper = np.array([60, 255, 255], np.uint8)
    # white color
    white_lower = np.array([0, 0, 200], np.uint8)
    white_upper = np.array([180, 20, 255], np.uint8)
    # black color
    black_lower = np.array([0, 0, 0], np.uint8)
    black_upper = np.array([180, 255, 30], np.uint8)
    # all color together
    red = cv2.inRange(hsv, red_lower, red_upper)
    blue = cv2.inRange(hsv, blue_lower, blue_upper)
    yellow = cv2.inRange(hsv, yellow_lower, yellow_upper)
    white = cv2.inRange(hsv, white_lower, white_upper)
    black = cv2.inRange(hsv, black_lower, black_upper)

    # Morphological Transform, Dilation
    kernal = np.ones((5, 5), "uint8")
    red = cv2.dilate(red, kernal)
    res_red = cv2.bitwise_and(img, img, mask=red)
    blue = cv2.dilate(blue, kernal)
    res_blue = cv2.bitwise_and(img, img, mask=blue)
    yellow = cv2.dilate(yellow, kernal)
    res_yellow = cv2.bitwise_and(img, img, mask=yellow)
    white = cv2.dilate(white, kernal)
    res_white = cv2.bitwise_and(img, img, mask=white)
    black = cv2.dilate(black, kernal)
    res_black = cv2.bitwise_and(img, img, mask=black)

    # circle parameters
    minDist = 100
    param1 = 30  # 500
    param2 = 50  # 200 #smaller value-> more false circles
    minRadius = 5
    maxRadius = 100  # 10

    # Tracking red
    contours, hierarchy = cv2.findContours(red, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    max_area = 0
    best_cnt = None
    # get the biggest contour
    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if area > max_area:
            max_area = area
            best_cnt = contour
    if best_cnt is not None:
        # check if the contour is a circle
        circles = cv2.HoughCircles(
            blurred,
            cv2.HOUGH_GRADIENT,
            1,
            minDist,
            param1=param1,
            param2=param2,
            minRadius=minRadius,
            maxRadius=maxRadius,
        )
        if circles is not None:
            # draw the outer circle
            x, y, w, h = cv2.boundingRect(best_cnt)
            print(f"Red circle detected at: x:{x} y:{y}")
            img = cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
            cv2.putText(
                img, "Rouge: ", (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255)
            )
            cv2.putText(
                img,
                "area: " + str(area),
                (x, y + 20),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.7,
                (255, 255, 255),
            )
            cv2.putText(
                img,
                f"x: {x} y: {y}",
                (x, y + 40),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.7,
                (255, 255, 255),
            )

    # Displaying the frame
    cv2.imshow("Color Tracking", img)
    if cv2.waitKey(10) & 0xFF == ord("q"):
        cap.release()
        cv2.destroyAllWindows()
        break
