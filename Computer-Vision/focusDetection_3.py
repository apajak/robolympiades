#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2023/01/31
# version ='3.0'
# ---------------------------------------------------------------------------
""" Python Focus detection for puck height detection """
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import cv2
import numpy as np

# open camera
cap = cv2.VideoCapture(0)

while True:
    # read image
    ret, img = cap.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # get the image width and height
    height, width, channels = img.shape
    distance = 20
    # keep only sharp pixels
    sharp_mask = cv2.Laplacian(gray, cv2.CV_8U, ksize=5)
    # convert to binary
    binary_mask = cv2.threshold(sharp_mask, 200, 255, cv2.THRESH_BINARY)[1]

    # isolate pixels get the neighbor color
    dilated_mask = cv2.dilate(binary_mask, None)
    eroded_mask = cv2.erode(dilated_mask, None)

    # apply morphology to remove isolated extraneous noise
    # use borderconstant of black since foreground touches the edges
    kernel = np.ones((3,3), np.uint8)
    morphologyEx = cv2.morphologyEx(eroded_mask, cv2.MORPH_OPEN, kernel)
    morphologyEx = cv2.morphologyEx(morphologyEx, cv2.MORPH_CLOSE, kernel)

    # anti-alias the mask -- blur then stretch
    # blur alpha channel
    blured_mask = cv2.GaussianBlur(morphologyEx, (0,0), sigmaX=2, sigmaY=2, borderType = cv2.BORDER_DEFAULT)


    contours, hierarchy = cv2.findContours(
        blured_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
    )
    # keep only the biggest contour
    biggest_contour = sorted(contours, key=cv2.contourArea, reverse=True)[:1]
    # draw contours
    cv2.drawContours(img, biggest_contour, -1, (0, 255, 0), 3)
    # drow the bounding box of the biggest contour
    for cnt in biggest_contour:
        x, y, w, h = cv2.boundingRect(cnt)
        if w > 100 and h > 100:
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
            # write the height of the bounding box
            cv2.putText(
                img, str(h), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2
            )

    cv2.imshow("focus_Exclusion", img)

    if cv2.waitKey(1) & 0xFF == ord("q"):
        break

# close camera
cap.release()
cv2.destroyAllWindows()