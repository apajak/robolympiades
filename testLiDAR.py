#!/usr/bin/env python3
"""Animates distances and measurment quality"""
from rplidar import RPLidar
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation

PORT_NAME = "/dev/cu.SLAB_USBtoUART"
DMAX = 3000
IMIN = 0
IMAX = 50


def update_point(num, iterator, point):
    scan = next(iterator)
    offsets = np.array([(np.radians(meas[1]), meas[2]) for meas in scan])
    point.set_offsets(offsets)
    intens = np.array([meas[0] for meas in scan])
    point.set_array(intens)
    return point


def run():
    lidar = RPLidar(PORT_NAME)
    fig = plt.figure()
    ax = plt.subplot(111, projection="polar")
    point = ax.scatter([0, 0], [0, 0], s=5, c=[IMIN, IMAX], cmap=plt.cm.Greys_r, lw=0)
    ax.set_rmax(DMAX)
    ax.grid(True)

    iterator = lidar.iter_scans()
    _ = animation.FuncAnimation(fig, update_point, fargs=(iterator, point), interval=50)
    plt.show()
    lidar.stop()
    lidar.disconnect()


if __name__ == "__main__":
    run()
