#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: Pajak Alexandre
# date: 2022-11-23
# version: 0.1
# description: Lidar detection
# import the necessary packages
from rplidar import RPLidar
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
from time import sleep
import threading

# global variables
PORT_NAME = "/dev/cu.SLAB_USBtoUART"

lidar = RPLidar(PORT_NAME)
scan_data = []

# draw window for live displaying the lidar data
def draw_window():
    # create a figure
    fig = plt.figure()
    # create a subplot
    ax = fig.add_subplot(111, projection="polar")
    # create a line
    (line,) = ax.plot([], [], "o-", lw=2)
    # set the axis limits
    ax.set_rmax(4000)
    ax.grid(True)
    # create the animation
    _ = animation.FuncAnimation(fig, update_plot, fargs=(line,), interval=50)
    # show the plot
    plt.show()


# update the plot
def update_plot(frame, line):
    # get the global variables
    global scan_data
    # get the data
    data = scan_data
    # get the angles
    angles = [x[1] for x in data]
    # get the distances
    distances = [x[2] for x in data]
    # set the data
    line.set_data(angles, distances)
    # return the line
    return line


# get the lidar data
def get_lidar_data():
    # get the global variables
    global scan_data
    # create a thread for getting the lidar data
    t = threading.Thread(target=get_lidar_data_thread)
    # start the thread
    t.start()
    # draw the window
    draw_window()


# get the lidar data thread
def get_lidar_data_thread():
    # get the global variables
    global scan_data
    # get the lidar data
    for scan in lidar.iter_scans():
        # set the scan data
        scan_data = scan
        # break the loop
        break


# main function
def main():
    # connect to the lidar
    lidar.connect()
    # start the lidar
    lidar.start_motor()
    # get the lidar data
    get_lidar_data()
    # stop the lidar
    lidar.stop_motor()
    # disconnect from the lidar
    lidar.disconnect()


# call the main function
if __name__ == "__main__":
    main()
